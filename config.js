module.exports = {
    platform: 'gitlab',
    endpoint: 'https://gitlab.com/api/v4/',
    autodiscover: true,
    autodiscoverFilter: "tintok/*",
    baseBranches: ['master'],
    labels: ['renovate'],
    rangeStrategy: 'replace',
    packageRules: [{
        managers: ["docker-compose"],
        updateTypes: ["pin", "digest"],
        enabled: false
    }],
    extends: ['config:base']
};